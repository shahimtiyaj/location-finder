package com.imtiyaj.android.allinone;


import android.app.Activity;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.imtiyaj.android.allinone.DatabaseHelper.SQLiteHandler;
import com.imtiyaj.android.allinone.Utils.VolleyCustomRequest;
import com.imtiyaj.android.allinone.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nusrat-pc on 10/19/16.
 */
public class Registration extends Activity {
    private static final String TAG = Registration.class.getSimpleName();
    ProgressBar progressBar;
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
    private SQLiteHandler db;
    private static final int SELECT_PICTURE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        inputFullName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                ArrayList<String> dataset = new ArrayList<String>();

                final String name = inputFullName.getText().toString().trim();
                final String email = inputEmail.getText().toString().trim();
                final String pass = inputPassword.getText().toString().trim();


                if (!name.isEmpty() && !email.isEmpty() && !pass.isEmpty()) {

                    long userID = db.addUser(name, email, pass);

                    dataSendToServer(name, email, pass);


                    if (userID > 0) {
                        Toast.makeText(getApplicationContext(),
                                "Registration Completed" + "\n" + "Your id is: " + userID, Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Registration Unsuccessful", Toast.LENGTH_LONG)
                                .show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }


            }


        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                String name = inputFullName.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                Intent intent = new Intent(Registration.this, Login.class);
                startActivity(intent);
            }
        });
    }



    private void dataSendToServer(String name, String email, String pass) {

        String hitURL = "http://shahimtiyaj94.comule.com/create_user.php";

        HashMap<String, String> params = new HashMap<>();
        params.put("name", name); //Items - Item 1 - name
        params.put("email", email); //Items - Item 2 - email
        params.put("pass", pass); //Items - Item 3 - pass

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int success = response.getInt("success");
                            if (success == 1) {


                                Toast.makeText(getApplicationContext(),
                                        response.getString("message"), Toast.LENGTH_SHORT).show();


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );

        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(postRequest);
    }



    // -----OR-----
    private class DoAsync extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... arg0) {


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

}








