package com.imtiyaj.android.allinone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.imtiyaj.android.allinone.DatabaseHelper.SQLiteHandler;


/**
 * Created by nusrat-pc on 10/19/16.
 */
public class Login extends Activity {

    private static final String TAG = Login.class.getSimpleName();
    private Button btnLogin, showbtn;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private SQLiteHandler db;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
                    // login user

                    if (db.checkEmailPassword(email, password)) {
                        Toast.makeText(Login.this, "Logging....", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(Login.this, MenuActivity.class));
                    }
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the right information!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // Link to Login Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Registration.class);
                startActivity(intent);
                finish();
            }
        });


    }
}



