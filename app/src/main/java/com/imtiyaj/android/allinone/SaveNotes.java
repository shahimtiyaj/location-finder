package com.imtiyaj.android.allinone;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.imtiyaj.android.allinone.Adapter.ProfileListAdapter;
import com.imtiyaj.android.allinone.DatabaseHelper.SQLiteHandler;
import com.imtiyaj.android.allinone.Model.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nusrat-pc on 11/2/16.
 */
public class SaveNotes extends Activity {

    private static final String TAG = SQLiteHandler.class.getSimpleName();
    private static final String TABLE_USER = "user";

    SQLiteHandler sqliteHandler;
    private List<UserInfo> userinformation;

    private ProfileListAdapter adapter;
    private ListView listView;

    private Context context = SaveNotes.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_list);

        userinformation = new ArrayList<UserInfo>();

        adapter = new ProfileListAdapter(this, userinformation);

        listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(adapter);

        sqliteHandler = new SQLiteHandler(context);

        showData();

    }

    private void showData() {

        userinformation.clear();

        String query = "SELECT * FROM " + TABLE_USER;

        SQLiteDatabase db = sqliteHandler.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {

                String title = cursor.getString(cursor.getColumnIndex("name"));
                String body = cursor.getString(cursor.getColumnIndex("email"));

                UserInfo userinfo2 = new UserInfo(title, body);

                userinfo2.setName(title);
                userinfo2.setEmail(body);

                Toast.makeText(context, title + " " + body, Toast.LENGTH_SHORT).show();

                userinformation.add(userinfo2);


            } while (cursor.moveToNext());

            adapter.notifyDataSetChanged();

        }
    }
}

