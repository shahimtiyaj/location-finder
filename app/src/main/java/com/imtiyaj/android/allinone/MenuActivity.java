package com.imtiyaj.android.allinone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


/**
 * Created by nusrat-pc on 10/19/16.
 */
public class MenuActivity extends Activity {

    private static final String TAG = Login.class.getSimpleName();
    private Button btnNearby;
    private Button btnEmergency;
    private Button btnRemainder;;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        btnNearby = (Button) findViewById(R.id.btnNearby);
        btnEmergency = (Button) findViewById(R.id.btnEmergency);
        btnRemainder = (Button) findViewById(R.id.btnRemainder);
        // Near By places on click listner
        btnNearby.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, MapsActivity.class);
                startActivity(intent);
                finish();

                    Toast.makeText(getApplicationContext(),
                            "Near By Places!", Toast.LENGTH_LONG)
                            .show();

            }

        });

        // Emergency places listener
        btnEmergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(),
                        "Emergency services", Toast.LENGTH_LONG)
                        .show();
               // Intent intent = new Intent(getApplicationContext(), Registration.class);
               // startActivity(intent);
                finish();
            }
        });

        // Remainder on click listner
        btnRemainder.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                Toast.makeText(getApplicationContext(),
                        "Remainder!", Toast.LENGTH_LONG)
                        .show();

            }

        });


    }
}



