package com.imtiyaj.android.allinone;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /****** Create Thread that will sleep for 5 seconds *************/
        Thread background = new Thread() {
            public void run() {

                try {
                    // Thread will sleep for 5 seconds
                    sleep(5 *1000);

                    // After 5 seconds redirect to another intent
                    Intent i = new Intent(getBaseContext(), Registration.class);
                    startActivity(i);

                    // Remove activity
                    finish();

                } catch (Exception e) {

                }
            }
        };
        background.start();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }



}
