package com.imtiyaj.android.allinone;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.imtiyaj.android.allinone.DatabaseHelper.SQLiteHandler;
import java.util.ArrayList;

/**
 * Created by nusrat-pc on 10/19/16.
 */
public class NotesAdd extends Activity {
    private static final String TAG = Registration.class.getSimpleName();
    private Button btnAdd;
    private EditText inputTitle;
    private EditText inputBody;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notes_writing_layout);

        inputTitle = (EditText) findViewById(R.id.title);
        inputBody = (EditText) findViewById(R.id.body);
        btnAdd = (Button) findViewById(R.id.btnAdd);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Register Button Click event
        btnAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                ArrayList<String> dataset = new ArrayList<String>();

                final String title = inputTitle.getText().toString().trim();
                final String body = inputBody.getText().toString().trim();


                if (!title.isEmpty() && !body.isEmpty()) {


                    long userID = db.addUser(title, body,null);


                    if (userID > 0) {
                        Toast.makeText(getApplicationContext(),
                                "Add to list" + "\n" + "Your note id is: " + userID, Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Don't Add to the list", Toast.LENGTH_LONG)
                                .show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please write something!", Toast.LENGTH_LONG)
                            .show();
                }


            }


        });


    }

}








