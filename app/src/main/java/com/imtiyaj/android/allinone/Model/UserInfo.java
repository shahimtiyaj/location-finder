package com.imtiyaj.android.allinone.Model;

/**
 * Created by nusrat-pc on 10/20/16.
 */

public class UserInfo {

    private String name;
    private String email;
    private String pass;


    public UserInfo(String name, String email){
        this.name=name;
        this.email=email;
       // this.pass=pass;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
